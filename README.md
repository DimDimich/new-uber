## How to launch
``` JSON
npm install
export CONFIG_PATH=<full path to config>
npm start
```

## Curl examples

#### Book trip

``` BASH
curl -XPOST -H "Content-type: application/json" -d '{
    "start_address": "1452 56th Street, Brooklyn, NY 11219",
    "end_address": "1452 53th Street, Brooklyn, NY 11219"
}' 'http://new-uber.x33-space.pro/trips'
```

#### Estimate price for 2 addresses

``` BASH
curl -XGET 'http://new-uber.x33-space.pro/estimates/price?start_address=1452 56th Street, Brooklyn, NY 11219&end_address=1452 53th Street, Brooklyn, NY 11219'
```

#### List trips (count & offset are optional, defaults: count=20, offset=0)

``` BASH
curl -XGET 'http://new-uber.x33-space.pro/trips?count=10&offset=0'
```

## TODO
- add lookup for environment (dev, stage, live)
- configure CI/CD (jenkins | bitbucket pipeline)
- go to typescript
- add swagger (generation of documentation)
- add elk (elasticsearch-logstash-kibana)
- refactor project/code structure, improve design pattern usage
- improve error messages
- improve logging
- consider routes naming/style
- configure mongodb sharding(depends on application usage)
