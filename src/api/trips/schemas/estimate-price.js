const Joi = require('joi');

module.exports = Joi.object().keys({
  start_address: Joi.string().required(),
  end_address: Joi.string().required(),
});