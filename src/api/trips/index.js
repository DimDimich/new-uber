const router = require('express').Router();
const queryValidator = require('../../core/query-validation-middleware');
const payloadValidator = require('../../core/payload-validation.middleware');
const estimatePriceQuerySchema = require('./schemas/estimate-price');
const paginationQuerySchema = require('../../core/schemas/pagination');
const bodyParser = require('body-parser');

module.exports = function(config, models) {
  const trips = require('./controllers')(config, models);

  router.get(
    '/trips',
    queryValidator(paginationQuerySchema),
    trips.fetch
  );

  router.post(
    '/trips',
    bodyParser.json(),
    payloadValidator(estimatePriceQuerySchema),
    trips.create
  );

  router.get(
    '/estimates/price',
    queryValidator(estimatePriceQuerySchema),
    trips.estimatePrice
  );

  return router;
};
