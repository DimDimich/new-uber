const estimatePrice = require('../../lib/common/estimate-price-by-addreses');

module.exports = function(config, { TripModel }) {

  const service_name = 'NewUberX';
  const discount = 0.2;

  return {
    fetch: async (req, res) => {
      try {
        const results = await TripModel.find({}, null, {
          skip: req.query.offset,
          limit: req.query.count
        });
        res.json(results);
      } catch (error) {
        res.status(500).end();
      }
    }, // fetch

    create: async (req, res) => {
      try {
        const payload = req.body;
        const estimate = await estimatePrice(payload, config.google.server_key, config.uber.app_key, discount);
        const trip = new TripModel(Object.assign(estimate, payload, { service_name }));
        await trip.save();
        res.json(trip.toObject());
      } catch (error) {
        if (error.type === 'UBER_API') {
          res.status(422).json(error);
        } else {
          console.log(error);
          res.status(500).json();
        }
      }
    }, // create

    estimatePrice: async (req, res) => {
      try {
        const payload = req.query;
        const estimate = await estimatePrice(payload, config.google.server_key, config.uber.app_key, discount);
        res.json(Object.assign(estimate, payload, { service_name }));
      } catch (error) {
        if (error.type === 'UBER_API') {
          res.status(422).json(error);
        } else {
          console.log(error);
          res.status(500).json();
        }
      }
    } // estimate price
  };
}
