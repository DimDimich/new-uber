const { Schema } = require('mongoose');

module.exports = function(db_connection) {
  const TripSchema = new Schema({
    price: { type: Number },
    start_address: { type: String },
    end_address: { type: String },
    start_latitude: { type: Number },
    start_longitude: { type: Number },
    end_latitude: { type: Number },
    end_longitude: { type: Number },
    service_name: { type: String },
    currency_code: { type: String },
    __v: { type: Number, select: false}
  });

  return {
    TripModel: db_connection.model('Trip', TripSchema)
  }
};
