const { get } = require('request-promise');
const queryString = require('query-string');

module.exports.price = async function price({ 
    start_latitude, start_longitude,
    end_latitude, end_longitude,
  }, app_key) {

  try {
    const qs = queryString.stringify({
      start_latitude,
      start_longitude,
      end_latitude,
      end_longitude
    });
    const uri = `https://api.uber.com/v1.2/estimates/price?${qs}`;

    const result = await get(uri, {
      headers: {
        'Content-Type': 'application/json',
        'Accept-Language': 'en_US',
        'Authorization': `Token ${app_key}`
      },
      json: true,
    });
    return result;
  } catch (error) {
    if (error.response.body.code === 'distance_exceeded') {
      throw {
        type: 'UBER_API',
        error: 'DistanceExceeded',
        message: error.error.message
      };
    }
  }
}