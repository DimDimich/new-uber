const { get } = require('request-promise');
const queryString = require('query-string');

module.exports.code = async function code(address, key) {
  const qs = queryString.stringify({ address, key });
  const uri = `https://maps.googleapis.com/maps/api/geocode/json?${qs}`;

  const response = await get(uri, { json: true });
  if (response.results.length < 1) {
    throw response.status;
  }
  return {lat, lng} = response.results[0].geometry.location;
}

