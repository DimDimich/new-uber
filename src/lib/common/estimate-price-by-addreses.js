const Promise = require('bluebird');
const uberEstimates = require('../uber-api/estimates');
const googleGeoconding = require('../google-api/geocoding');

module.exports = async function({start_address, end_address}, google_key, uber_key, discount) {
  const [start, end] = await Promise.all([
    await googleGeoconding.code(start_address, google_key),
    await googleGeoconding.code(end_address, google_key)
  ]);

  const result = await uberEstimates.price({
    start_latitude: start.lat,
    start_longitude: start.lng,
    end_latitude: end.lat,
    end_longitude: end.lng
  }, uber_key);

  const { high_estimate, low_estimate, currency_code } = result.prices[0];
  const avarage_price = (high_estimate + low_estimate) / 2;
  const price = avarage_price - (avarage_price * discount);
  return {
    price, currency_code,
    start_latitude: start.lat,
    start_longitude: start.lng,
    end_latitude: end.lat,
    end_longitude: end.lng
  };
}