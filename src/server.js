const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const internalErrorMiddleware = require('./core/server-error-middleware');

module.exports = function(config) {
  const mongodb = require('./db/mongo-connection')(config);

  const models = {
    TripModel: require('./models/trip')(mongodb).TripModel
  };

  const app = express();

  app.use(morgan('dev'));
  app.use(bodyParser.urlencoded({ extended: false }))
  
  app.use(require('./api/trips')(config, models));
  app.use(internalErrorMiddleware);
  
  return app;
};