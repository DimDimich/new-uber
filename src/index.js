const http = require('http');
const config = require('./config')(process.env.CONFIG_PATH);
const app = require('./server')(config);

http
.createServer(app)
.listen(config.server.port, err => {
  if (err) throw err;
  console.log(`Server has been started on http://localhost:${config.server.port}`);
});
