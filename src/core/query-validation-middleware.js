const Joi = require('joi');

module.exports = function(joi_schema) {
  return function(req, res, next) {
    const {error, value} = Joi.validate(req.query, joi_schema)
    if (error) {
      res.status(400).json(error);
    } else {
      req.query = value;
      next();
    }
  }
}