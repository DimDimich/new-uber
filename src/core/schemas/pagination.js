const Joi = require('joi');

module.exports = Joi.object().keys({
  count: Joi.number().integer().min(0).max(10000).default(20),
  offset: Joi.number().integer().min(0).default(0),
});