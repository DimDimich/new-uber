const Joi = require('joi');

module.exports = Joi.object().keys({
  server: Joi.object().keys({
    port: Joi.number().integer()
  }).requiredKeys(['port']),
  uber: Joi.object().keys({
    app_key: Joi.string()
  }).requiredKeys(['app_key']),
  google: Joi.object().keys({
    server_key: Joi.string()
  }).requiredKeys(['server_key']),
  mongodb: Joi.object().keys({
    user: Joi.string(),
    password: Joi.string(),
    url: Joi.string(),
    db: Joi.string(),
  }).requiredKeys(['user', 'password', 'url', 'db'])
});