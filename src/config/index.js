const Joi = require('joi');

module.exports = function(config_path) {
  const config_schema = require('./config.schema');
  const config = require(config_path);
  try {
    const {error, value} = Joi.validate(config, config_schema);
    if (error) { throw error; }
    return value;
  } catch (err) {
    console.log(error);
    process.exit(1);
  }
}

