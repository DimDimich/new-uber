var mongoose = require('mongoose');

module.exports = function(config) {
  const { user, password, url, db} = config.mongodb;

  let options = {
    promiseLibrary: require('bluebird'),
    useMongoClient: true,
    user: user,
    pass: password
  };

  let connection = mongoose.createConnection(`mongodb://${url}/${db}`, options);
  
  connection.on('error', console.error.bind(console, 'connection error:'));

  connection.once('open', function() {
    console.log(`MongoDB connected on ${url}/${db} for user ${user}`);
  });

  return connection;
};
